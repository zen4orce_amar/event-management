public class venuepopulate {
     @AuraEnabled
    public static Campaign getcampignList(Id recordId){
         system.debug('hello');
        Campaign campaign=[select Venue__r.Name from Campaign where id=:recordId];
        system.debug('option'+campaign);
        return campaign;
    }
    
     @AuraEnabled
    public static List<Venue__c> getsubvenueList(String accId) {
        //system.debug('accId+++++'+accId);
        Campaign campaign = [select Venue__c, Venue__r.Name,Venue__r.ParentVenue__r.Name, Venue__r.Seating_Capacity__c,Venue__r.Type__c,Venue__r.Active__c from Campaign where id=:accId];
        List<Venue__c> venueList = new List<Venue__c>();
       if (campaign.Venue__r.ParentVenue__r == null) {
            venueList.add(campaign.Venue__r);
        } else {
            venueList.add(campaign.Venue__r.ParentVenue__r);
            venueList.add(campaign.Venue__r);
            
        }
        system.debug('venueList'+venueList);
        //Id =:option.Venue__c OR 
        List<Venue__c> subVenueList = [select Id,Name,ParentVenue__r.Name,Seating_Capacity__c,Type__c,Active__c from Venue__c where ParentVenue__c =: campaign.Venue__c];
         system.debug('subVenueList'+subVenueList);
        if (subVenueList != null && subVenueList.size() > 0) {
            venueList.addAll(subVenueList);
        }
        
        system.debug('venueList :: '+venueList);        
      
        return venueList;
    }
    
     @AuraEnabled
     public static Event_Venue__c savevenue(Event_Venue__c emp) {
        system.debug('empsspop++'+emp);
        
        return emp;
    }
    
     @AuraEnabled
    public static void some() {
        system.debug('accId+++++sss');
       
       
    }

}