public class venuelistapex {
     @AuraEnabled
    public static List<Venue__c> findVenueByName(String searchKey) {
        String name = '%' + searchKey + '%';
        String city = '%' + searchKey + '%';
        return [SELECT Id, Name,Seating_Capacity__c,Type__c,Active__c FROM Venue__c WHERE (Name LIKE :name OR City__c LIKE :city) AND ParentVenue__c =: null];
    }
    @AuraEnabled
    public static List<Venue__c> getsubVenueList(List<string> subvenueId) {
        List<Venue__c> resultList = [select Id,Name,ParentVenue__r.Name,Seating_Capacity__c,Type__c,Active__c from Venue__c where ParentVenue__c IN: subvenueId];
        return resultList;
    }
      @AuraEnabled
    public static List<Venue__c> getSelectedvenues(Id recordId) {
        list<Id> idList = new list<Id>();
        List<Event_Venue__c > resultList = [select Venue__r.Name, Venue__c from Event_Venue__c where Campaign__c =: recordId];
        for(Event_Venue__c vObj :resultList ){
            idList.add(vObj.Venue__c);
        }
        list<Venue__c> veneuList = [SELECT Id, Name,Seating_Capacity__c,Type__c,Active__c FROM Venue__c where Id IN : idList];
        return veneuList;
        
    }
    @AuraEnabled
    public static String deletevenues(Id recordId,Id venRecordId) {
        List<Event_Venue__c > resultList = [select Campaign__r.Start_Date__c,Campaign__r.End_Date__c, Venue__r.Name, Venue__c from Event_Venue__c where Venue__c =: venRecordId and Campaign__c =:recordId];
        System.debug('resultList'+resultList);
        date todaysDate = System.today();
        date startDate = resultList[0].Campaign__r.Start_Date__c.date();
        date endDate = resultList[0].Campaign__r.End_Date__c.date();
        //System.debug('todaysDate'+todaysDate);
       // System.debug('startDate'+startDate);
        //System.debug('endDate'+endDate);
        String result;
        if(todaysDate > startDate && todaysDate > endDate){
            delete resultList;
            result ='Succes';
        }else if(todaysDate >= startDate || todaysDate <= endDate){
            System.debug('Error');
            result ='Error';
        }
        return result;
    }
    
    
    
    
}