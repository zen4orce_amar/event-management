public class DrawLayoutController {
     public static String LAYOUT_TYPE_ROUND = 'Round';
    public static String LAYOUT_TYPE_SQUARE = 'Square';
    public static String LAYOUT_TYPE_AUDIENCE = 'Audience';
    public static String LAYOUT_TYPE_CLASSROOM = 'Classroom';
    public static String LAYOUT_TYPE_BOOTH = 'Booth';
   
    
    @AuraEnabled
    public Static List<Layout__mdt> getLayoutTypes()
    {
		List<Layout__mdt> layoutTypeList = [ SELECT DeveloperName
                                               FROM Layout__mdt ];
        
        return layoutTypeList;
    }

   
    @AuraEnabled
    public Static  List<Custom__mdt> getCustomLayoutTypes()
    {
        List<Custom__mdt> customLayoutTypeList = [SELECT Label, DeveloperName 
                                                    FROM Custom__mdt ];
       
 		return customLayoutTypeList; 
           
    }
     
    
    @AuraEnabled
    public Static  List<Plain__mdt> getPlainLayoutTypes()
    {
		List<Plain__mdt> plainLayoutTypeList = [SELECT DeveloperName 
                                                  FROM Plain__mdt ];
       
 		return plainLayoutTypeList; 
    }


   
    @AuraEnabled
    public Static List<String> getConferenceTypes()
    {
        List<String> conferenceTypeList = new List<String>();
        Schema.DescribeFieldResult fieldResult = Venue_Layout__c.Conference_Type__c.getDescribe();
        List<Schema.PicklistEntry> picklistValues = fieldResult.getPicklistValues();
        
        for (Schema.PicklistEntry value: picklistValues) {
            conferenceTypeList.add(value.getLabel());
        }
        
        return conferenceTypeList;
    }
    
     @AuraEnabled
    public Static  DesignLayoutWrapper saveVenueLayout(String eventvenueId,Venue_Layout__c layout){
        system.debug('eventvenueId'+eventvenueId);
        if (layout == null) {
            return null;
        }
		Event_Venue__c eventVenue = [
            SELECT Id
                 , Campaign__c
            	 , Campaign__r.Name
            	 , Venue__c
                 , Venue__r.Name
                 , Venue_Layout__c
                 , Venue_Layout__r.Name
              FROM Event_Venue__c
            where Id =: eventVenueId
        ];

		Venue_Layout__c venueLayoutobj = new Venue_Layout__c();
        venueLayoutobj = [SELECT Id,
                       Type__c,
                       Layout__c,
                          Sections__c,
                      No_of_Seats__c,
                       No_of_Tables__c,
                       Layout_Row_Count__c,
                      Horizontal_Seats__c,
                       LayoutDetails__c,
                       Seats_Per_Row__c
                       from Venue_Layout__c WHERE Id =: eventVenue.Venue_Layout__c ];
        system.debug('layout.LayoutDetails__c'+venueLayoutobj.LayoutDetails__c);
        DesignLayoutWrapper  returnWrapper = createLayoutJson(venueLayoutobj);        
        
        String layoutDetailsJson = '';
        String customtype = 'custom';
         
        DesignLayoutWrapper designLayoutWrapper = new DesignLayoutWrapper();
        if(venueLayoutobj.Type__c!=layout.Type__c){
             designLayoutWrapper.defaultLayoutType = customtype;
        } else {
	         designLayoutWrapper.defaultLayoutType = layout.Type__c;
        }
        
        designLayoutWrapper.guid = generateGUID();
        
       /* if(layout.Sections__c != null){
         
            designLayoutWrapper.Sections = layout.Sections__c != null ? Integer.valueOf(layout.Sections__c) : 0 ;
        }else{
            designLayoutWrapper.Sections = venueLayoutobj.Sections__c != null ? Integer.valueOf(venueLayoutobj.Sections__c) : 0 ;
        }*/

        if (layout.Type__c == LAYOUT_TYPE_ROUND || layout.Type__c == LAYOUT_TYPE_SQUARE) {
            designLayoutWrapper.layoutRowCount = layout.Layout_Row_Count__c != null ? Integer.valueOf(layout.Layout_Row_Count__c) : 0 ;
        designLayoutWrapper.totalSeats = layout.No_of_Seats__c != null ? Integer.valueOf(layout.No_of_Seats__c) : 0;
		designLayoutWrapper.SeatsPerRow = designLayoutWrapper.layoutRowCount;//layout.Seats_Per_Row__c != null ? Integer.valueOf(layout.Seats_Per_Row__c) : 1 ;

            designLayoutWrapper.numberOfTables = layout.No_of_Tables__c != null ? Integer.valueOf(layout.No_of_Tables__c) : 0;
            DesignLayoutWrapper.Table[] tableList = new DesignLayoutWrapper.Table[] { };

            // Creating table layout
            createTableLayout(tableList, layout, designLayoutWrapper, returnWrapper);

            designLayoutWrapper.tables = new DesignLayoutWrapper.Table[] {};
            designLayoutWrapper.tables.addAll(tableList);
            designLayoutWrapper.tables.addAll(returnWrapper.tables);
			designLayoutWrapper.numberOfTables = designLayoutWrapper.tables.size();
            // create a JSON structure and store in the venue layout
            layoutDetailsJson =  JSON.serialize(designLayoutWrapper);

            System.debug('JSON ::: '+ layoutDetailsJson);

        } else if (layout.Type__c == LAYOUT_TYPE_CLASSROOM || layout.Type__c == LAYOUT_TYPE_BOOTH ) {
            DesignLayoutWrapper.Table[] tableList = new DesignLayoutWrapper.Table[] { };
            DesignLayoutWrapper.Miscellaneous[] miscellaneouslist = new DesignLayoutWrapper.Miscellaneous[] { };
            designLayoutWrapper.tables = new DesignLayoutWrapper.Table[] {};
        
            designLayoutWrapper.layoutRowCount = venueLayoutobj.Layout_Row_Count__c != null ? Integer.valueOf(venueLayoutobj.Layout_Row_Count__c) : 0 ;
            designLayoutWrapper.totalSeats = layout.No_of_Seats__c != null ? Integer.valueOf(layout.No_of_Seats__c) : 0;
            designLayoutWrapper.SeatsPerRow = venueLayoutobj.Layout_Row_Count__c != null ? Integer.valueOf(venueLayoutobj.Layout_Row_Count__c) : 1 ;
            designLayoutWrapper.numberOfTables = 1;
        	  
            DesignLayoutWrapper.Table table = new DesignLayoutWrapper.Table();
            table.layoutType = layout.Type__c;
            table.tableName  = layout.Type__c +'-A'; 
            table.label      = layout.Type__c +'-A';
            table.guid       = generateGUID();
            table.numberOfSeats = designLayoutWrapper.totalSeats;//Integer.valueOf(layout.No_of_Seats__c);
			table.SeatsPerRow = layout.Layout_Row_Count__c != null ? Integer.valueOf(layout.Layout_Row_Count__c) : 1 ;
            designLayoutWrapper.tables.add(table);
            designLayoutWrapper.tables.addAll(returnWrapper.tables);
            designLayoutWrapper.numberOfTables = designLayoutWrapper.tables.size();
            designLayoutWrapper.miscellaneous = new DesignLayoutWrapper.Miscellaneous[] {};
            designLayoutWrapper.miscellaneous.addAll(miscellaneouslist);
            layoutDetailsJson =  JSON.serialize(designLayoutWrapper);

            System.debug('JSON ::: '+ layoutDetailsJson);
            
        } else if (layout.Type__c == LAYOUT_TYPE_AUDIENCE ) {
            DesignLayoutWrapper.Table[] tableList = new DesignLayoutWrapper.Table[] { };
            DesignLayoutWrapper.Miscellaneous[] miscellaneouslist = new DesignLayoutWrapper.Miscellaneous[] { };
            designLayoutWrapper.tables = new DesignLayoutWrapper.Table[] {};
            designLayoutWrapper.layoutRowCount = layout.Layout_Row_Count__c != null ? Integer.valueOf(layout.Layout_Row_Count__c) : 0 ;
            designLayoutWrapper.totalSeats = layout.No_of_Seats__c != null ? Integer.valueOf(layout.No_of_Seats__c) : 0;
            designLayoutWrapper.SeatsPerRow = layout.Seats_Per_Row__c != null ? Integer.valueOf(layout.Seats_Per_Row__c) : 1 ;
            designLayoutWrapper.numberOfTables = 1;
    
            DesignLayoutWrapper.Table table = new DesignLayoutWrapper.Table();
            table.layoutType = layout.Type__c;
            table.tableName  = layout.Type__c +'-A'; 
            table.label      = layout.Type__c +'-A';
            table.guid       = generateGUID();
            table.numberOfSeats = designLayoutWrapper.totalSeats;//Integer.valueOf(layout.No_of_Seats__c);
			table.SeatsPerRow = layout.Seats_Per_Row__c != null ? Integer.valueOf(layout.Seats_Per_Row__c) : 1 ;
            table.sectionsType = layout.Sections__c != null ? Integer.valueOf(layout.Sections__c) : 0 ;
            designLayoutWrapper.tables.add(table);
            designLayoutWrapper.tables.addAll(returnWrapper.tables);
            designLayoutWrapper.numberOfTables = designLayoutWrapper.tables.size();
            
            designLayoutWrapper.miscellaneous = new DesignLayoutWrapper.Miscellaneous[] {};
            designLayoutWrapper.miscellaneous.addAll(miscellaneouslist);
            layoutDetailsJson =  JSON.serialize(designLayoutWrapper);

            System.debug('JSON ::: '+ layoutDetailsJson);
            
        }else if (layout.Type__c == 'ConferenceHall' ){
        	if(layout.Conference_Type__c == 'U-Shaped'){
        	system.debug('U-shaped');
            DesignLayoutWrapper.Table[] tableList = new DesignLayoutWrapper.Table[] { };
            DesignLayoutWrapper.Miscellaneous[] miscellaneouslist = new DesignLayoutWrapper.Miscellaneous[] { };
            designLayoutWrapper.tables = new DesignLayoutWrapper.Table[] {};
        
            designLayoutWrapper.layoutRowCount = venueLayoutobj.Layout_Row_Count__c != null ? Integer.valueOf(venueLayoutobj.Layout_Row_Count__c) : 0 ;
            designLayoutWrapper.totalSeats = layout.No_of_Seats__c != null ? Integer.valueOf(layout.No_of_Seats__c) : 0;
            designLayoutWrapper.SeatsPerRow = venueLayoutobj.Layout_Row_Count__c != null ? Integer.valueOf(venueLayoutobj.Layout_Row_Count__c) : 1 ;
            designLayoutWrapper.numberOfTables = 1;
        	    
            DesignLayoutWrapper.Table table = new DesignLayoutWrapper.Table();
            table.layoutType = layout.Type__c;
            table.tableName  = layout.Type__c +'-A' +layout.Conference_Type__c; 
            table.label      = layout.Type__c +'-A';
            table.guid       = generateGUID();
            table.numberOfSeats = designLayoutWrapper.totalSeats;//Integer.valueOf(layout.No_of_Seats__c);
			table.SeatsPerRow = venueLayoutobj.Layout_Row_Count__c != null ? Integer.valueOf(venueLayoutobj.Layout_Row_Count__c) : 1 ;
			table.HorizontalSeatsBottom =  layout.Horizontal_Seats__c != null ? Integer.valueOf(layout.Horizontal_Seats__c) : 0;// A S K I I :: Assign field value layout.HorizontalSeats
           table.defaultConferenceType = layout.Conference_Type__c;
            designLayoutWrapper.tables.add(table);
    		designLayoutWrapper.tables.addAll(returnWrapper.tables);   
                 designLayoutWrapper.numberOfTables = designLayoutWrapper.tables.size();
            designLayoutWrapper.miscellaneous = new DesignLayoutWrapper.Miscellaneous[] {};
            designLayoutWrapper.miscellaneous.addAll(miscellaneouslist);
            layoutDetailsJson =  JSON.serialize(designLayoutWrapper);

            System.debug('JSON ::: '+ layoutDetailsJson);
        	}
				else{
        	system.debug('closed and open');
            DesignLayoutWrapper.Table[] tableList = new DesignLayoutWrapper.Table[] { };
            DesignLayoutWrapper.Miscellaneous[] miscellaneouslist = new DesignLayoutWrapper.Miscellaneous[] { };
            designLayoutWrapper.tables = new DesignLayoutWrapper.Table[] {};
        
            designLayoutWrapper.layoutRowCount = venueLayoutobj.Layout_Row_Count__c != null ? Integer.valueOf(venueLayoutobj.Layout_Row_Count__c) : 0 ;
            designLayoutWrapper.totalSeats = layout.No_of_Seats__c != null ? Integer.valueOf(layout.No_of_Seats__c) : 0;
            designLayoutWrapper.SeatsPerRow = venueLayoutobj.Layout_Row_Count__c != null ? Integer.valueOf(venueLayoutobj.Layout_Row_Count__c) : 1 ;
            designLayoutWrapper.numberOfTables = 1;
        	  
            DesignLayoutWrapper.Table table = new DesignLayoutWrapper.Table();
            table.layoutType = layout.Type__c;
            table.tableName  = layout.Type__c +'-A' +layout.Conference_Type__c; 
            table.label      = layout.Type__c +'-A';
            table.guid       = generateGUID();
            table.numberOfSeats = designLayoutWrapper.totalSeats;//Integer.valueOf(layout.No_of_Seats__c);
			table.SeatsPerRow = venueLayoutobj.Layout_Row_Count__c != null ? Integer.valueOf(layout.Layout_Row_Count__c) : 1 ;
			table.HorizontalSeatsTop  =  layout.Horizontal_Seats__c != null ? Integer.valueOf(layout.Horizontal_Seats__c) : 0;
			table.HorizontalSeatsBottom =  layout.Horizontal_Seats__c != null ? Integer.valueOf(layout.Horizontal_Seats__c) : 0;// A S K I I :: Assign field value layout.HorizontalSeats
           table.defaultConferenceType = layout.Conference_Type__c;
            designLayoutWrapper.tables.add(table);
           	designLayoutWrapper.tables.addAll(returnWrapper.tables);
           	designLayoutWrapper.numberOfTables = designLayoutWrapper.tables.size();
            designLayoutWrapper.miscellaneous = new DesignLayoutWrapper.Miscellaneous[] {};
            designLayoutWrapper.miscellaneous.addAll(miscellaneouslist);
            layoutDetailsJson =  JSON.serialize(designLayoutWrapper);

            System.debug('JSON ::: '+ layoutDetailsJson);
        	}
        } 
       
   		 Venue_Layout__c venueLayout = new Venue_Layout__c();
       

        try {
			venueLayout.Id = eventVenue.Venue_Layout__c;
            venueLayout.LayoutDetails__c = layoutDetailsJson;
            venueLayout.Type__c = layout.Type__c;
            venueLayout.Layout__c = layout.Layout__c;
         
            venueLayout.No_of_Tables__c = layout.No_of_Tables__c;
           
            venueLayout.No_of_Seats__c = layout.No_of_Seats__c;
            venueLayout.Layout_Row_Count__c = layout.Layout_Row_Count__c;
            venueLayout.Conference_Type__c = layout.Conference_Type__c;
            venueLayout.Horizontal_Seats__c = layout.Horizontal_Seats__c;
             venueLayout.Seats_Per_Row__c = layout.Seats_Per_Row__c;
            venueLayout.Sections__c = layout.Sections__c;
            
            venueLayout.Venue__c = eventVenue.Venue__c;
            update venueLayout;
           
            system.debug('venueLayout//////////////////////'+venueLayout);
        } catch(Exception ex) {
            System.debug('Exception :: ' + ex.getMessage());
        }
        
		DesignLayoutWrapper wrapperObject = new DesignLayoutWrapper();
        wrapperObject = createLayoutJson(venueLayout);//(DesignLayoutWrapper) JSON.deserializeStrict(venueLayoutObj.LayoutDetails__c,DesignLayoutWrapper.class);
        System.debug('wrapperObject' + wrapperObject);
        return wrapperObject;
		
    }
    
    public static void createTableLayout(DesignLayoutWrapper.Table[] tableList, Venue_Layout__c layout, DesignLayoutWrapper designLayoutWrapper, DesignLayoutWrapper olddesignLayoutWrapper) {
        Integer tableCount = 1;
        String alphaSequence = 'A';
        Integer positionX = 200;
        Integer positionY = 200;
        Integer seats = layout.No_of_Seats__c != null ? Integer.valueOf(layout.No_of_Seats__c) : 0;
        Integer tables = layout.No_of_Tables__c != null ? Integer.valueOf(layout.No_of_Tables__c) : 0;
        Integer tableSeatRemainedCount = Math.mod(seats,  tables);
        Integer tableSeatCount = seats / tables;
        System.debug('seats :: ' + seats);
        System.debug('tables :: ' + tables);
        System.debug('tableSeatCount :: ' + tableSeatCount);
        
        
        While (tableCount <= designLayoutWrapper.numberOfTables) {
            DesignLayoutWrapper.Table table = new DesignLayoutWrapper.Table();
            table.layoutType = layout.Type__c;
            table.tableName  = alphaSequence + (tableCount + olddesignLayoutWrapper.tables.size());
            table.label      = alphaSequence + (tableCount + olddesignLayoutWrapper.tables.size());
            table.guid       = generateGUID();
            
            if (tableSeatRemainedCount > 0 && tableCount == designLayoutWrapper.numberOfTables) {
                table.numberOfSeats = tableSeatCount + tableSeatRemainedCount;//Integer.valueOf(layout.No_of_Seats__c);
            } else {
            	table.numberOfSeats = tableSeatCount;//Integer.valueOf(layout.No_of_Seats__c);    
            }

            if (layout.Type__c == LAYOUT_TYPE_SQUARE) {
                table.height = '';
                table.width  = '';
            }

            DesignLayoutWrapper.Position position = new DesignLayoutWrapper.Position();
            position.positionX = positionX + 'px';
            position.positionY = positionY + 'px';
            table.position = position;

            tableList.add(table);
            tableCount++;
            positionX += 100;
            positionY += 100;
        }
        
        
    }

    public static String generateGUID() {
        return EncodingUtil.convertToHex(Crypto.generateDigest('MD5', Blob.valueOf(DateTime.now().getTime().format())));
    }
    
    public static DesignLayoutWrapper createLayoutJson(Venue_Layout__c venueLayoutobj) {
         system.debug('venueLayoutobj'+venueLayoutobj.LayoutDetails__c);
         DesignLayoutWrapper wrapperObject = new DesignLayoutWrapper();
         wrapperObject = (DesignLayoutWrapper) JSON.deserializeStrict(venueLayoutobj.LayoutDetails__c, DesignLayoutWrapper.class);
        
			system.debug('wrapperObject'+wrapperObject);
         return wrapperObject;
     }
     
     

}