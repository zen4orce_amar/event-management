public class VenueLayoutArrangementController {
    @AuraEnabled
    public static DesignLayoutWrapper getVenueLayoutType(String venueLayoutId){
        
       
        Event_Venue__c evtVenue = [Select Id,
                                  Venue_Layout__c from Event_Venue__c where Id =: venueLayoutId LIMIT 1];
        
        System.debug('evtVenue' + evtVenue);
        
        System.debug('evtVenue' + evtVenue);
        
        Venue_Layout__c venueLayout = new Venue_Layout__c();
        venueLayout = [SELECT Id,
                           Type__c,
                           Layout__c,
                              No_of_Seats__c,
                              No_of_Tables__c,
                              Layout_Row_Count__c,
                              Horizontal_Seats__c,
                              LayoutDetails__c,
                              Seats_Per_Row__c
                              from Venue_Layout__c WHERE Id =: evtVenue.Venue_Layout__c ];//'a017F000000diKoQAI' ] ;
        DesignLayoutWrapper wrapperObject = new DesignLayoutWrapper();
        wrapperObject = (DesignLayoutWrapper) JSON.deserializeStrict(venueLayout.LayoutDetails__c, DesignLayoutWrapper.class);
        System.debug('wrapperObject' + wrapperObject);
        return wrapperObject;
    }
    
    /* A S K I I
  : Retriving header details for which requires Event Venue.. 
 */
    @AuraEnabled
    public static Event_Venue__c getEventHeaderDetails(String eventVenueId){
  if (eventVenueId == null) {
            return null;
        } 
   Event_Venue__c eventVenue = [
            SELECT Id
                 , Campaign__c
              , Campaign__r.Name
              , Venue__c
                 , Venue__r.Name
                 , Venue_Layout__c
                 , Venue_Layout__r.Name
              FROM Event_Venue__c
            where Id =: eventVenueId
        ];    
        return eventVenue != null ? eventVenue : new Event_Venue__c();
    }
    
    @AuraEnabled
    public static Venue__c getEventVenues(String eventvenueId){
        if (eventVenueId == null) {
            return null;
        } 
   Event_Venue__c eventVenue = [
            SELECT Id
                 , Campaign__c
              , Campaign__r.Name
              , Venue__c
                 , Venue__r.Name
                 , Venue_Layout__c
                 , Venue_Layout__r.Name
              FROM Event_Venue__c
            where Id =: eventVenueId
        ];  
        if(eventVenue!= null){
      Venue__c venue = [
          SELECT Id
          ,Name
         ,Street_Line_1__c
          ,Street_Line_2__c
          ,City__c
          ,Country__c
          ,Zipcode__c
          ,Location__c
          ,Seating_Capacity__c
          ,Active__c
          ,Type__c 
          FROM Venue__c 
          where id = :eventVenue.Venue__c];
        system.debug('venue' +venue);
        return venue;
        }else{
             return null;
        }
       
    }

    @AuraEnabled
    public static DesignLayoutWrapper saveVenueLayout(String evtVenueId,List<DesignLayoutWrapper.Table> updatedJSON,Integer totalSeats,String updateWrapperObjectList){
        
        
        System.debug('evtVenueId' + evtVenueId);
        System.debug('updatedJSON' + updatedJSON);
        System.debug('totalSeats' + totalSeats);
        System.debug('updateObj' + updateWrapperObjectList);
        
        DesignLayoutWrapper designLayoutWrapperObject = (DesignLayoutWrapper) JSON.deserialize(updateWrapperObjectList, DesignLayoutWrapper.class) ;
        
        System.debug('designLayoutWrapperList' + designLayoutWrapperObject.tables );
        
        Event_Venue__c evtVenue = [Select Id,
                                      Venue_Layout__c from Event_Venue__c where Id =: evtVenueId LIMIT 1];
        Venue_Layout__c venueLayout = new Venue_Layout__c();
        venueLayout = [SELECT Id,
                              Type__c,
                              Layout__c,
                              No_of_Seats__c,
                              No_of_Tables__c,
                              Layout_Row_Count__c,
                              Horizontal_Seats__c,
                              LayoutDetails__c,
                              Seats_Per_Row__c
                              from Venue_Layout__c WHERE Id =: evtVenue.Venue_Layout__c ];
        if(venueLayout!= null){
            String layoutJSON = '';
            layoutJSON = JSON.serialize(designLayoutWrapperObject);
            venueLayout.LayoutDetails__c = layoutJSON;
         //   venueLayout.No_of_Seats__c = totalSeats;
            upsert venueLayout;
        }
        return getVenueLayoutType(evtVenueId);
    }
    @AuraEnabled 
    public static void saveSeatAllocatedMembers(string fieldList) {
        
         System.debug('fieldLis');
      System.debug('fieldList'+fieldList);
        List<Event_Registration__c> eventReg;
    try {
        eventReg = (List<Event_Registration__c>)JSON.deserialize(fieldList, List<Event_Registration__c>.class);
    } catch (Exception e) {
        
    }
          insert eventReg;
     
     }
     
     @AuraEnabled
    public static Map<String,List<Event_Registration__c>> getEventRegistrationContact(String eventvenueId){
        
       
        List<Event_Registration__c> evtRegistrationList = [Select Id,
                                                           Contact__c,Event_Venue__c,Seat__c,Status__c,Venue__c from Event_Registration__c where Event_Venue__c = : eventvenueId];
        Map<String,List<Event_Registration__c>> tableMap = new  Map<String,List<Event_Registration__c>>();
        for(Event_Registration__c eventResObj : evtRegistrationList){
            
            list<string> seatList = eventResObj.Seat__c.split('_');
            string seatNo = seatList[0];    
          if(tableMap.containsKey(seatNo)){
               
               tableMap.get(seatNo).add(eventResObj);
            }
            else{
                
                tableMap.put(seatNo, new list<Event_Registration__c>{eventResObj});
            }   
        }
        system.debug('****************tableMap******************'+tableMap);
        
        return tableMap;
        
    }
   
}