public class venueController {
    
    @AuraEnabled
    public static List<Venue__c > fetchVenue(String searchKeyWord) {
  String searchKey = searchKeyWord + '%';
  List <Venue__c> returnList = new List <Venue__c> ();
  List <Venue__c> lstOfAccount = [select Id,Name,ParentVenue__c,Seating_Capacity__c,Type__c,City__c from Venue__c where Name LIKE: searchKey];
 system.debug('lstOfAccount'+lstOfAccount);
  for (Venue__c acc: lstOfAccount) {
   returnList.add(acc);
  }
  return returnList;
 }
     
 @AuraEnabled
    public static List<Venue__c> getsubvenueList(String accId) {
        //system.debug('accId+++++'+accId);
        List<Venue__c> options = [select Id,Name,ParentVenue__r.Name,Seating_Capacity__c from Venue__c where ParentVenue__c =:accId];
        system.debug('options'+options);
      
        return options;
    }
    
    
    @AuraEnabled
     public static Event_Venue__c savevenue(Event_Venue__c emp) {
        system.debug('empss++'+emp);
        
        return emp;
    }
    
}