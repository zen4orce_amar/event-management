({
	createConference : function(component, event,helper) {
	   
       debugger;
       var tableName = component.get("v.wrapperObj.tableName");
       var globalID = component.getGlobalId();
       console.log(component.getGlobalId());
        
       var obj =  component.get("v.wrapperObj");
       var parentdiv = document.getElementById(globalID); 
       parentdiv.addEventListener("click",$A.getCallback(function myFunction(component,event){
       var sObjectEvent = $A.get("e.c:EditLayoutEvent");
       		sObjectEvent.setParams({
            "message": obj
            })
        sObjectEvent.fire();
        }));
       var totalNoOfSeats = component.get("v.wrapperObj.NumberOfSeats");
       var horizontalSeats = component.get("v.wrapperObj.HorizontalSeatsBottom");
        //component.get("v.wrapperObj.NumberOfSeats");
        var numberOfSeatsOnLeftSide = Math.round((totalNoOfSeats - horizontalSeats)/2) ;
        var numberOfSeatsOnRightSide = totalNoOfSeats - numberOfSeatsOnLeftSide - horizontalSeats ;
        
        // left Side seating arrangement start 
        
        var getLeftSeats = document.getElementById(globalID+'_leftSeating');
        var leftSideSeats = document.createElement('ul');
        leftSideSeats.id = globalID + '_leftSideUl';
        getLeftSeats.appendChild(leftSideSeats);
        var i=0; 
        
        for(i=0;i<numberOfSeatsOnLeftSide;i++){
            var leftSideSeat =  document.createElement('li');
            leftSideSeat.id = globalID + '_leftSideSeat'+i;
            leftSideSeat.className = 'chair';
            leftSideSeat.id = tableName+'_S'+i;
            $(leftSideSeat).on('dragover', function (event) {
                console.log('ondragover0 called...');
                event.preventDefault();
            });
            
            $(leftSideSeat).on('drop', function (event) {
                var sObjectEvent1;
	                var obje = {};
	          		 var status = $(this).attr('class');
	                if(status.indexOf("booked") <= 0){
	                    var listitem = '';
	                   if(event.originalEvent != undefined && event.originalEvent != null){
	                    	listitem = event.originalEvent.dataTransfer.getData("obj");    
	                        obje = JSON.parse(listitem);
	                    }
	                    var contactId = obje.Id;
	                    var contactName = obje.Name;
	                    component.set("v.afterRemove",contactName);
	                    var seatNo = $(this).find('p').text();
	                    var tableNo = component.get("v.wrapperObj.tableName");
	                    var seat = tableNo + '_'+ seatNo;
	                    var conSeat = contactName + '[' + seat + ']';
	                    component.set("v.afterRemove",conSeat);
	                    $(this).addClass('booked');
	                    $(this).addClass('bookedseat');
	                    $(this).attr('data','booked');
	                    $(this).attr('contact',contactId);
	                    var conList = component.get("v.contactList");
	                    for(var i = 0;i<conList.length;i++){
	                        if(conList[i].Name == contactName){
	                            conList[i].Name = contactName + '[' + seat + ']';
	                            conList[i].Title = seat;
	                        }
	                    }
	                    
	                    $(this).append('<span class="orangeBox ' + seat + '">X</span>');
                       
                       var spanId = document.getElementsByClassName(seat);
	                    console.log('spanId'+spanId);
	                    
	                     $(this).hover(
	                      function () {
	                       $(spanId).show();
	                      }, 
	                      function () {
	                        $(spanId).hide();
	                      }
	                    );
	                    
						$(spanId).on('click' ,function(){
	                   	//alert('Hello');
	                        debugger;
	                        $(this).parent().removeClass('bookedseat');
	                        $(this).parent().removeClass('booked');
	                        $(this).parent().removeAttr('data');
	                        $(this).parent().unbind('mouseenter');
	                        $(this).parent().removeClass('orangeBox');
	                        $(spanId).remove();
	
	                        var a = $(this).parent();
	                        var contactID = a.attr("contact")
	                        var conList = component.get("v.contactList");
	                    	for(var i = 0;i<conList.length;i++){
	                        	if(conList[i].Id == contactID){
	                            	//conList[i].Name = 'WWWWWW';//Mustafa will look into it
	                            	//conList[i].Title = '';
	                        	}
	                    	}
	                        //$(this).parent().trigger("drop");
	                                                 
	                         sObjectEvent1 = component.getEvent("getAssignSeat");
	                    
	                            sObjectEvent1.setParams({
	                                "contactId":contactId,
	                                "seat" :seat,
	                                "tableNo":tableNo,
	                                "status":"Cancelled"
	                               })
	                           sObjectEvent1.fire();
	                      });
	                     sObjectEvent1 = component.getEvent("getAssignSeat");
	                    var status = "Registered";
	                    sObjectEvent1.setParams({
	                        "contactId":contactId,
	                        "seat" :seat,
	                        "tableNo":tableNo,
	                        "status":status
	                       })
	                   sObjectEvent1.fire();
	                }
	                else{
	                    alert("Sorry ! already booked...");
	                }
            });// End Drop 
            $("ul[id='"+globalID+"_leftSideUl"+"']").append(leftSideSeat);
            var seatNumber = document.createElement('p');
            seatNumber.innerHTML = 'S'+i;
            seatNumber.className = 'p';
            leftSideSeat.appendChild(seatNumber);
            
            
        }
        var lSeats = document.getElementById(globalID+'_leftSeating');
        lSeats.style.height = 30*i+'px';
        // left Side seating arrangement end
        
        // Horizontal seat arrangement Start
        var getHorizontalSeats = document.getElementById(globalID+'_horizontalSeating');
        var horizontalSideSeats = document.createElement('ul');
        horizontalSideSeats.id = globalID + '_horizontalUl';
        getHorizontalSeats.appendChild(horizontalSideSeats);
        var k=0;
        for(k=0;k<horizontalSeats;k++){
            var q= i+k;
            
            var horizontalSeat =  document.createElement('li');
            horizontalSeat.id = globalID + '_horizontalSeat'+k;
            horizontalSeat.className = 'chair';
            horizontalSeat.id = tableName+'_S'+q;
            $(horizontalSeat).on('dragover', function (event) {
                console.log('ondragover0 called...');
                event.preventDefault();
            });
            
            $(horizontalSeat).on('drop', function (event) {
                var sObjectEvent1;
	                var obje = {};
	          		 var status = $(this).attr('class');
	                if(status.indexOf("booked") <= 0){
	                    var listitem = '';
	                   if(event.originalEvent != undefined && event.originalEvent != null){
	                    	listitem = event.originalEvent.dataTransfer.getData("obj");    
	                        obje = JSON.parse(listitem);
	                    }
	                    var contactId = obje.Id;
	                    var contactName = obje.Name;
	                    component.set("v.afterRemove",contactName);
	                    var seatNo = $(this).find('p').text();
	                    var tableNo = component.get("v.wrapperObj.tableName");
	                    var seat = tableNo + '_'+ seatNo;
	                    var conSeat = contactName + '[' + seat + ']';
	                    component.set("v.afterRemove",conSeat);
	                    $(this).addClass('booked');
	                    $(this).addClass('bookedseat');
	                    $(this).attr('data','booked');
	                    $(this).attr('contact',contactId);
	                    var conList = component.get("v.contactList");
	                    for(var i = 0;i<conList.length;i++){
	                        if(conList[i].Name == contactName){
	                            conList[i].Name = contactName + '[' + seat + ']';
	                            conList[i].Title = seat;
	                        }
	                    }
	                    
	                    $(this).append('<span class="orangeBox ' + seat + '">X</span>');
                       
                       var spanId = document.getElementsByClassName(seat);
	                    console.log('spanId'+spanId);
	                    
	                     $(this).hover(
	                      function () {
	                       $(spanId).show();
	                      }, 
	                      function () {
	                        $(spanId).hide();
	                      }
	                    );
	                    
						$(spanId).on('click' ,function(){
	                   	//alert('Hello');
	                        debugger;
	                        $(this).parent().removeClass('bookedseat');
	                        $(this).parent().removeClass('booked');
	                        $(this).parent().removeAttr('data');
	                        $(this).parent().unbind('mouseenter');
	                        $(this).parent().removeClass('orangeBox');
	                        $(spanId).remove();
	
	                        var a = $(this).parent();
	                        var contactID = a.attr("contact")
	                        var conList = component.get("v.contactList");
	                    	for(var i = 0;i<conList.length;i++){
	                        	if(conList[i].Id == contactID){
	                            	//conList[i].Name = 'WWWWWW';//Mustafa will look into it
	                            	//conList[i].Title = '';
	                        	}
	                    	}
	                        //$(this).parent().trigger("drop");
	                                                 
	                         sObjectEvent1 = component.getEvent("getAssignSeat");
	                    
	                            sObjectEvent1.setParams({
	                                "contactId":contactId,
	                                "seat" :seat,
	                                "tableNo":tableNo,
	                                "status":"Cancelled"
	                               })
	                           sObjectEvent1.fire();
	                      });
	                     sObjectEvent1 = component.getEvent("getAssignSeat");
	                    var status = "Registered";
	                    sObjectEvent1.setParams({
	                        "contactId":contactId,
	                        "seat" :seat,
	                        "tableNo":tableNo,
	                        "status":status
	                       })
	                   sObjectEvent1.fire();
	                }
	                else{
	                    alert("Sorry ! already booked...");
	                }
            }); // End Drop
            $("ul[id='"+globalID+"_horizontalUl"+"']").append(horizontalSeat);
            var seatNumber = document.createElement('p');
            seatNumber.innerHTML = 'S' +q;
            seatNumber.className = 'p';
            horizontalSeat.appendChild(seatNumber);
        }
        var hSeats = document.getElementById(globalID+'_horizontalSeating');
        hSeats.style.width = 50+25*k+'px';
        // Horizontal seat arrangement end
        // Right Side seating arrangement start
        var getRightSeats = document.getElementById(globalID+'_rightSeating');
        var rightSideSeats = document.createElement('ul');
        rightSideSeats.id = globalID + '_rightSideUl';
        getRightSeats.appendChild(rightSideSeats);
        var j=0; 
        for(j=0;j<numberOfSeatsOnRightSide;j++){
            debugger;
            //q+numberOfSeatsOnRightSide-j
            var r= q+numberOfSeatsOnRightSide-j;
            var rightSideSeat =  document.createElement('li');
            rightSideSeat.id = globalID + '_rightSideSeat'+j;
            rightSideSeat.className = 'chair';
            rightSideSeat.id = tableName+'_S'+r;
            $(rightSideSeat).on('dragover', function (event) {
                console.log('ondragover0 called...');
                event.preventDefault();
            });
            
            $(rightSideSeat).on('drop', function (event) {
                var sObjectEvent1;
	                var obje = {};
	          		 var status = $(this).attr('class');
	                if(status.indexOf("booked") <= 0){
	                    var listitem = '';
	                   if(event.originalEvent != undefined && event.originalEvent != null){
	                    	listitem = event.originalEvent.dataTransfer.getData("obj");    
	                        obje = JSON.parse(listitem);
	                    }
	                    var contactId = obje.Id;
	                    var contactName = obje.Name;
	                    component.set("v.afterRemove",contactName);
	                    var seatNo = $(this).find('p').text();
	                    var tableNo = component.get("v.wrapperObj.tableName");
	                    var seat = tableNo + '_'+ seatNo;
	                    var conSeat = contactName + '[' + seat + ']';
	                    component.set("v.afterRemove",conSeat);
	                    $(this).addClass('booked');
	                    $(this).addClass('bookedseat');
	                    $(this).attr('data','booked');
	                    $(this).attr('contact',contactId);
	                    var conList = component.get("v.contactList");
	                    for(var i = 0;i<conList.length;i++){
	                        if(conList[i].Name == contactName){
	                            conList[i].Name = contactName + '[' + seat + ']';
	                            conList[i].Title = seat;
	                        }
	                    }
	                    
	                   $(this).append('<span class="orangeBox ' + seat + '">X</span>');
                       
                       var spanId = document.getElementsByClassName(seat);
	                    console.log('spanId'+spanId);
	                    
	                     $(this).hover(
	                      function () {
	                       $(spanId).show();
	                      }, 
	                      function () {
	                        $(spanId).hide();
	                      }
	                    );
	                    
						$(spanId).on('click' ,function(){
	                   	//alert('Hello');
	                        debugger;
	                        $(this).parent().removeClass('bookedseat');
	                        $(this).parent().removeClass('booked');
	                        $(this).parent().removeAttr('data');
	                        $(this).parent().unbind('mouseenter');
	                        $(this).parent().removeClass('orangeBox');
	                        $(spanId).remove();
	
	                        var a = $(this).parent();
	                        var contactID = a.attr("contact")
	                        var conList = component.get("v.contactList");
	                    	for(var i = 0;i<conList.length;i++){
	                        	if(conList[i].Id == contactID){
	                            	//conList[i].Name = 'WWWWWW';//Mustafa will look into it
	                            	//conList[i].Title = '';
	                        	}
	                    	}
	                        //$(this).parent().trigger("drop");
	                                                 
	                         sObjectEvent1 = component.getEvent("getAssignSeat");
	                    
	                            sObjectEvent1.setParams({
	                                "contactId":contactId,
	                                "seat" :seat,
	                                "tableNo":tableNo,
	                                "status":"Cancelled"
	                               })
	                           sObjectEvent1.fire();
	                      });
	                     sObjectEvent1 = component.getEvent("getAssignSeat");
	                    var status = "Registered";
	                    sObjectEvent1.setParams({
	                        "contactId":contactId,
	                        "seat" :seat,
	                        "tableNo":tableNo,
	                        "status":status
	                       })
	                   sObjectEvent1.fire();
	                }
	                else{
	                    alert("Sorry ! already booked...");
	                }
            });
            $("ul[id='"+globalID+"_rightSideUl"+"']").append(rightSideSeat);
            var seatNumber = document.createElement('p');
            seatNumber.innerHTML = 'S'+r;
            seatNumber.className = 'p';
            rightSideSeat.appendChild(seatNumber);
            
        }
        
        var lSeats = document.getElementById(globalID+'_rightSeating');
        lSeats.style.height = 30*i+'px';
        lSeats.style.marginTop = (30*i)*(-1)+'px';
        lSeats.style.marginLeft = 90+10*(horizontalSeats+1)+(24*horizontalSeats)+'px';
        
        this.eventRegistrationContactRound(component, event,helper);
    },
    
    eventRegistrationContactRound : function(component,event,helper){
        debugger;
         var tableName = component.get("v.wrapperObj.tableName");
         var globalID = component.getGlobalId();
      
        var eventRegistrationValues = component.get("v.eventRegistrationValues");

        if(eventRegistrationValues!=null){
          for(var i=0; i< eventRegistrationValues.length;i++){
        
	        var childId =   document.getElementById(eventRegistrationValues[i].Seat__c);
	        if(childId != null){
	            
	            childId.style.backgroundColor = "#D5A69B";
	        }else{
	            $(this).css('background-color','#D5A69B');
	        }
          }
        }
     }
})