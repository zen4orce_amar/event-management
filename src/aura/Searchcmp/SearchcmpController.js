({
    searchKeyChange: function(component, event, helper) {
        var myEvent = $A.get("e.c:SearchEvent");
        myEvent.setParams({"searchKey": event.target.value});
        myEvent.fire();
    },
    createRecord : function (component, event, helper) {
    var createRecordEvent = $A.get("e.force:createRecord");
    createRecordEvent.setParams({
        "entityApiName": "Venue__c"
    });
    createRecordEvent.fire();
    event.preventDefault();
	}
})