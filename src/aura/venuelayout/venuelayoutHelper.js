({
	callayout: function(component, event, helper){
        var action = component.get("c.getLayoutTypes");
        var inputsel = component.find("InputSelect");
      var opts=[];
    action.setCallback(this, function(a) {
        opts.push({"class": "optionClass", label:'--Select--', value: '--Select--'});
        for(var i=0;i< a.getReturnValue().length;i++){
            opts.push({"class": "optionClass", label: a.getReturnValue()[i].DeveloperName, value: a.getReturnValue()[i].DeveloperName});
        }
        inputsel.set("v.options", opts);

    });
    $A.enqueueAction(action); 

		
	},
   
    
    calconference: function(component, event, helper){
         var action = component.get("c.getConferenceTypes");
        var inputsel = component.find("conf");
      var opts=[];
    action.setCallback(this, function(a) {
        opts.push({"class": "optionClass", label:'--Select--', value: '--Select--'});
        for(var i=0;i< a.getReturnValue().length;i++){
            opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
        }
        inputsel.set("v.options", opts);

    });
    $A.enqueueAction(action); 
   
     
    },
    errormsg : function(component, event, helper){
          var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type" : "Error!",
                        "title": "Error!",
                        "message": "Fill all Feilds",
                    });
                    toastEvent.fire();
                 $A.util.addClass(search, 'slds-hide');
                $A.util.removeClass(search, 'slds-show');
      
        
    },
    doInitHelper : function(component,event,helper){
         var listOfVenues = component.get("v.venueIds");
         var recordId = component.get("v.recordId");
         var recordId = component.get("v.recordId");
        console.log('Init venueLayoutController');
        //component.set("v.flag",true);
        var opts=[];
        var action = component.get("c.getEventVenues");
        action.setParams({ "venueList" : listOfVenues,"recordId" : recordId});
        
        action.setCallback(this, function(response) {
            debugger;
            var state = response.getState();
            var results = response.getReturnValue();
            if (state === "SUCCESS") {
                
                component.set("v.venue", results);
               
            }
            
            
            
        });
        $A.enqueueAction(action);
        this.callayout(component, event,helper);
    },
    
     resetVenueLayoutSearch : function(component,event) {
         debugger;
        component.set("{!v.venuelayout.Layout__c}", "");
        component.set("{!v.venuelayout.Type__c}", "");
        component.set("{!v.venuelayout.No_of_Tables__c}", "");
        component.set("{!v.venuelayout.No_of_Seats__c}", "");
        component.set("{!v.venuelayout.Layout_Row_Count__c}", "");
        component.set("{!v.venuelayout.Conference_Type__c}", "");
          component.set("{!v.venuelayout.Horizontal_Seats__c}", "");
         component.set("{!v.venuelayout.Sections__c}", "");
           component.set("{!v.venuelayout.Seats_Per_Row__c}", "");
         
       // component.set("v.venuegrid", '');
      	  var assign= component.find("form1"); 
        $A.util.addClass(assign, 'slds-hide');
        $A.util.removeClass(assign, 'slds-show');
        //event.preventDefault();
    },
    
    validatefeilds : function(component,event,helper){
        debugger;
         var layoutType=component.get("v.inputsel");
       var flag = component.get("v.validateflag");
         var search= component.find("search"); 
         if(layoutType == "Round"){
              var tables = component.find("table").get("v.value");
            var seats = component.find("seat").get("v.value");
            var rows = component.find("row").get("v.value");
              var table = parseInt(component.find("table").get("v.value"));
            var seat = parseInt(component.find("seat").get("v.value"));
            var row = parseInt(component.find("row").get("v.value"));
            if(tables == undefined || seats == undefined || rows == undefined ){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error!",
                    "title": "Error!",
                    "message": "Fill all Feilds",
                });
                toastEvent.fire();
                
                $A.util.addClass(search, 'slds-hide');
                $A.util.removeClass(search, 'slds-show');
                flag = "true";
                component.set("v.validateflag", flag);
                
            }
              
               else if(table == 0 || seat == 0 || row == 0){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error!",
                    "title": "Error!",
                    "message": "Please provide valid data",
                });
                toastEvent.fire();
    		  $A.util.addClass(search, 'slds-hide');
                $A.util.removeClass(search, 'slds-show');
                   flag = "true";
                    component.set("v.validateflag", flag);
               
               }else if(table > seat){
                   var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error!",
                    "title": "Error!",
                    "message": "Seat count should be greater then Table count",
                });
                toastEvent.fire();
    		  $A.util.addClass(search, 'slds-hide');
                $A.util.removeClass(search, 'slds-show');
                   flag = "true";
                    component.set("v.validateflag", flag);
                    
                  
                   
               }
             /*else if(seat > 20){
                   var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error!",
                    "title": "Error!",
                    "message": "Max seat per table is 20",
                });
                toastEvent.fire();
    		  $A.util.addClass(search, 'slds-hide');
                $A.util.removeClass(search, 'slds-show');
                   flag = "true";
                    component.set("v.validateflag", flag);
                   
               }else if(row > "4"){
                   var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error!",
                    "title": "Error!",
                    "message": "Max column count is 4",
                });
                toastEvent.fire();
    		  $A.util.addClass(search, 'slds-hide');
                $A.util.removeClass(search, 'slds-show');
                   flag = "true";
                    component.set("v.validateflag", flag);
                   
               }*/else{
                   var flag = "false";
        	component.set("v.validateflag", flag);
               }
         }if(layoutType == "Square"){
              var tabless = component.find("tables").get("v.value");
            var seatss = component.find("seats").get("v.value");
            var rowss = component.find("rows").get("v.value");
               var tables = parseInt(component.find("tables").get("v.value"));
            var seats = parseInt(component.find("seats").get("v.value"));
            var rows = parseInt(component.find("rows").get("v.value"));
              if(tabless == undefined || seatss == undefined || rowss == undefined ){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error!",
                    "title": "Error!",
                    "message": "Fill all Feilds",
                });
                toastEvent.fire();
                $A.util.addClass(search, 'slds-hide');
                $A.util.removeClass(search, 'slds-show');
                flag = "true";
                component.set("v.validateflag", flag);
            }
              else if(tables == 0 || seats == 0 || rows == 0){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error!",
                    "title": "Error!",
                    "message": "Please provide valid data",
                });
                toastEvent.fire();
    		  $A.util.addClass(search, 'slds-hide');
                $A.util.removeClass(search, 'slds-show');
                   flag = "true";
                    component.set("v.validateflag", flag);
               
               }else if(seats < tables){
                   var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error!",
                    "title": "Error!",
                    "message": "Seat count should be greater then Table count",
                });
                toastEvent.fire();
    		  $A.util.addClass(search, 'slds-hide');
                $A.util.removeClass(search, 'slds-show');
                   flag = "true";
                    component.set("v.validateflag", flag);
                    
                  
                   
               }
             /*else if(seat > 20){
                   var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error!",
                    "title": "Error!",
                    "message": "Max seat per table is 20",
                });
                toastEvent.fire();
    		  $A.util.addClass(search, 'slds-hide');
                $A.util.removeClass(search, 'slds-show');
                   flag = "true";
                    component.set("v.validateflag", flag);
                   
               }else if(row > "4"){
                   var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error!",
                    "title": "Error!",
                    "message": "Max column count is 4",
                });
                toastEvent.fire();
    		  $A.util.addClass(search, 'slds-hide');
                $A.util.removeClass(search, 'slds-show');
                   flag = "true";
                    component.set("v.validateflag", flag);
                   
               }*/else{
                   var flag = "false";
        	component.set("v.validateflag", flag);
               }
         }
        if(layoutType == "ConferenceHall"){
               var conf = parseInt(component.find("conf").get("v.value"));
            
            var sea = parseInt(component.find("sea").get("v.value"));
            var per = parseInt(component.find("per").get("v.value"));
             var confs = component.find("conf").get("v.value");
            
            var seas = component.find("sea").get("v.value");
            var pers = component.find("per").get("v.value");
            if(confs == "--Select--"   || seas == undefined || pers == undefined){
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error!",
                    "title": "Error!",
                    "message": "Fill all Feilds",
                });
                toastEvent.fire();
                $A.util.addClass(search, 'slds-hide');
                $A.util.removeClass(search, 'slds-show');
                flag = "true";
                component.set("v.validateflag", flag);
            }
              else if(sea == 0 || per == 0 ){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error!",
                    "title": "Error!",
                    "message": "Please provide valid data",
                });
                toastEvent.fire();
    		  $A.util.addClass(search, 'slds-hide');
                $A.util.removeClass(search, 'slds-show');
                   flag = "true";
                    component.set("v.validateflag", flag);
               
               }else{
                   var flag = "false";
        	component.set("v.validateflag", flag);
               }
         }if(layoutType == "Audience"){
                var section = parseInt(component.find("section").get("v.value"));
            var set = parseInt(component.find("set").get("v.value"));
            var pers = parseInt(component.find("pers").get("v.value"));
            var ro = parseInt(component.find("ro").get("v.value"));
              var sections = component.find("section").get("v.value");
            var sets = component.find("set").get("v.value");
            var perss = component.find("pers").get("v.value");
            var ros = component.find("ro").get("v.value");
            
            if(sections == undefined   || sets == undefined || perss == undefined || ros == undefined){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error!",
                    "title": "Error!",
                    "message": "Fill all Feilds",
                });
                toastEvent.fire();
                $A.util.addClass(search, 'slds-hide');
                $A.util.removeClass(search, 'slds-show');
                flag = "true";
                component.set("v.validateflag", flag);
                
            }
               else if(section == 0 || set == 0 || pers == 0 || ro == 0){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error!",
                    "title": "Error!",
                    "message": "Please provide valid data",
                });
                toastEvent.fire();
    		  $A.util.addClass(search, 'slds-hide');
                $A.util.removeClass(search, 'slds-show');
                   flag = "true";
                    component.set("v.validateflag", flag);
               
               }else{
                   var flag = "false";
        	component.set("v.validateflag", flag);
               }
         }if(layoutType == "Classroom"){
               var set = parseInt(component.find("setcs").get("v.value"));
            var pers = parseInt(component.find("percs").get("v.value"));
             var sets = component.find("setcs").get("v.value");
            var perss = component.find("percs").get("v.value");
           
            
            if(sets == undefined || perss == undefined ){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error!",
                    "title": "Error!",
                    "message": "Fill all Feilds",
                });
                toastEvent.fire();
                $A.util.addClass(search, 'slds-hide');
                $A.util.removeClass(search, 'slds-show');
                flag = "true";
                component.set("v.validateflag", flag);
                
            }
               else if(set == 0 || pers == 0 ){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error!",
                    "title": "Error!",
                    "message": "Please provide valid data",
                });
                toastEvent.fire();
    		  $A.util.addClass(search, 'slds-hide');
                $A.util.removeClass(search, 'slds-show');
                   flag = "true";
                    component.set("v.validateflag", flag);
               
               }else{
                   var flag = "false";
        	component.set("v.validateflag", flag);
               }
         }if(layoutType == "Booth"){
               var set = parseInt(component.find("sebts").get("v.value"));
            var pers = parseInt(component.find("pebrs").get("v.value"));
             var sets = component.find("sebts").get("v.value");
            var perss = component.find("pebrs").get("v.value");
           
            
            if(sets == undefined || perss == undefined ){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error!",
                    "title": "Error!",
                    "message": "Fill all Feilds",
                });
                toastEvent.fire();
                $A.util.addClass(search, 'slds-hide');
                $A.util.removeClass(search, 'slds-show');
                flag = "true";
                component.set("v.validateflag", flag);
            }
              else  if(set == 0 || pers == 0 ){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Error!",
                    "title": "Error!",
                    "message": "Please provide valid data",
                });
                toastEvent.fire();
    		  $A.util.addClass(search, 'slds-hide');
                $A.util.removeClass(search, 'slds-show');
                   flag = "true";
                    component.set("v.validateflag", flag);
               
               }else{
                   var flag = "false";
        	component.set("v.validateflag", flag);
               }
         }
        
         }
})