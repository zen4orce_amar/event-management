({
    doInit : function(component, event, helper) {
        debugger;
        component.set("v.venue","");
        helper.doInitHelper(component,event,helper);
    },
	
    assign : function(component, event, helper) {
        debugger;
		
		var selectedItem = event.currentTarget;
		var inputsel = selectedItem.dataset.record;
		component.set("v.venueid", inputsel);
		
		var text= component.find("text");
		var assign= component.find("form2");
		var search= component.find("search");   
		var assign1= component.find("form1");
		
		$A.util.addClass(text, 'slds-hide');
		$A.util.removeClass(text, 'slds-show');

		$A.util.addClass(assign1, 'slds-hide');
		$A.util.removeClass(assign1, 'slds-show');      
		$A.util.removeClass(assign, 'slds-hide');
		$A.util.addClass(assign, 'slds-show');
		$A.util.addClass(search, 'slds-hide');
		$A.util.removeClass(search, 'slds-show');
		event.preventDefault();
    },
	
    onChangeFunction : function(component, event, helper){
        debugger
		var inputsel = component.find("InputSelect").get("v.value");
		var search= component.find("search");   
		var assign= component.find("form1");
		var text= component.find("text");
		$A.util.removeClass(assign, 'slds-hide');
		$A.util.addClass(assign, 'slds-show');

		$A.util.addClass(text, 'slds-hide');
		$A.util.removeClass(text, 'slds-show');  
		$A.util.addClass(search, 'slds-hide');
		$A.util.removeClass(search, 'slds-show');
		
		event.preventDefault();
		
		if(inputsel == "Custom"){
			var action = component.get("c.getCustomLayoutTypes");
		}else if(inputsel == "Plain"){
			var action = component.get("c.getPlainLayoutTypes");
		}else{
			return null;
		}
		
		var opts=[];
		action.setCallback(this, function(a) {
			opts.push({"class": "optionClass", label:'--Select--', value: '--Select--'});
			for(var i=0;i< a.getReturnValue().length;i++){
				opts.push({"class": "optionClass", label: a.getReturnValue()[i].Label, value: a.getReturnValue()[i].DeveloperName});
			}
			component.find("type").set("v.options", opts);

		});
		$A.enqueueAction(action); 
    },

    onChange : function(component, event, helper){
        debugger;
        var text= component.find("text");
        $A.util.removeClass(text, 'slds-hide');
        $A.util.addClass(text, 'slds-show');
        var search= component.find("search");   
        var inputsel = component.find("type").get("v.value");
        
		component.set("v.inputsel", inputsel);
        component.set("v.createb", inputsel);
        component.set("v.venuegrid", '');
        
		if(inputsel == 'ConferenceHall'){
            helper.calconference(component, event, helper);
            //component.set("v.flag",false);
        }
		
        $A.util.addClass(search, 'slds-hide');
        $A.util.removeClass(search, 'slds-show');
    },
	
    search : function(component, event, helper){
        debugger;
        //component.set("v.flag",false);
      
		var search= component.find("search");    
		var modal= component.find("modal");
		//var flag = "false";
		var flag=component.get("v.validateflag");
		var layoutType=component.get("v.inputsel");
		var recordId = component.get("v.recordId");
		var venueid = component.get("v.venueid");
		var action = component.get("c.matchvenuelayout");
		var layout=component.get("v.venuelayout");
     
        helper.validatefeilds(component, event,helper);
        var flag=component.get("v.validateflag");
        if(flag == "false"){
            
            action.setParams({ "layout" : layout,"recordId" : recordId, "venueid" : venueid,"layoutType" : layoutType});
            
            action.setCallback(this, function(response) {
                debugger;
                var state = response.getState();
                //var name=response.getReturnValue();
                if (state === "SUCCESS") {
                    $A.util.removeClass(search, 'slds-hide');
                    $A.util.addClass(search, 'slds-show');
                    debugger;
                    if(response.getReturnValue() != null && response.getReturnValue().length > 0){
                        
                        component.set("v.createb", layoutType);
                        component.set("v.venuegrid", response.getReturnValue());
                        //component.set("v.flag",true);
                    } else {
                        $A.util.addClass(search, 'slds-hide');
                        $A.util.removeClass(search, 'slds-show');
                        $A.util.removeClass(modal, 'slds-hide');
        				$A.util.addClass(modal, 'slds-show');
                    }
                }
            });
			
            $A.enqueueAction(action);
        }
    },
	
    Savenext : function(component,event,helper){
        debugger;
		var venueid = component.get("v.venueid");
		var layout=component.get("v.venuelayout");
		var assign= component.find("custom");
		var assign1= component.find("form2");
		var assign2= component.find("form1"); 
		var assign3= component.find("text"); 
		var assign4= component.find("search");
		var laybutton= component.find("layoutbutton");
		var action = component.get("c.saveEventVenueLayout");
		var recordId = component.get("v.recordId");
		var venueLayoutId = component.get("v.venueLayoutId");
		var venid= component.get("v.venuelayout");
		var listOfVenues = component.get("v.venue");
		var buttonid = component.get("v.venueid");
        
		if(venueLayoutId == undefined || venueLayoutId == null){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type" : "Error!",
                "title": "Error!",
                "message": "Please select record",
            });
            toastEvent.fire();
            event.preventDefault();
            
        } else {
            action.setParams({ "recordId" : recordId,"venueLayoutId" : venueLayoutId
                              ,"venueid" : venueid});
            
            action.setCallback(this, function(response) {
                debugger;
                var state = response.getState();
                  var eventvenue = response.getReturnValue();
                if (state === "SUCCESS") {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type" : "Success!",
                        "title": "Success!",
                        "message": "Record created Successfully",
                    });
                    toastEvent.fire();
                    event.preventDefault();
                    $A.util.addClass(assign1, 'slds-hide');
					$A.util.removeClass(assign1, 'slds-show');
                    $A.util.addClass(assign2, 'slds-hide');
					$A.util.removeClass(assign2, 'slds-show');
                    $A.util.addClass(assign3, 'slds-hide');
					$A.util.removeClass(assign3, 'slds-show');
                    $A.util.addClass(assign4, 'slds-hide');
					$A.util.removeClass(assign4, 'slds-show');
					if(buttonid!= null) { 
						// var outerDivID =component.find(buttonid);
					  
						$A.util.removeClass(assign, 'slds-hide');
						$A.util.addClass(assign, 'slds-show');
						$A.util.addClass(laybutton, 'slds-hide');
						$A.util.removeClass(laybutton, 'slds-show');
						event.preventDefault();
						if (listOfVenues == undefined || listOfVenues == null) {
							return;
						}
						listOfVenues.forEach(function(record) {
							console.log(record);
							if (record.Id == buttonid) {
								record.IsCustomized = true;
							}
						});
						component.set("v.venue", listOfVenues);
						component.set("v.eventvenueId", eventvenue.Id);
					}
					
                    helper.resetVenueLayoutSearch(component);
                }
            });
			
            $A.enqueueAction(action);
        }
    },
	
    cancel : function(component,event,helper) {
        var assign= component.find("form2"); 
        $A.util.addClass(assign, 'slds-hide');
        $A.util.removeClass(assign, 'slds-show');
        event.preventDefault();
        helper.resetVenueLayoutSearch(component);
    },
	
    clickFunction : function(component,event,helper){
        debugger;
        var selectedItem = event.currentTarget;
        var inputsel = selectedItem.dataset.record;
        component.set("v.venueLayoutId", inputsel);
    },
	
    create : function(component,event,helper){
        debugger;
		var venueid = component.get("v.venueid");
		var action = component.get("c.createEventVenueLayout");
		var layout=component.get("v.venuelayout");
		var recordId = component.get("v.recordId");
		var assign= component.find("custom");
		var assign1= component.find("form2");
		var assign2= component.find("form1"); 
		var assign3= component.find("text"); 
		var assign4= component.find("search");
		var laybutton= component.find("layoutbutton");
		var buttonid = component.get("v.venueid");
		var listOfVenues = component.get("v.venue");
		var modal= component.find("modal");

        action.setParams({ "layout" : layout,"recordId" : recordId,"venueid" : venueid});
        action.setCallback(this, function(response) {
            debugger;
            var state = response.getState();
            var eventvenue = response.getReturnValue();
            if (state === "SUCCESS") {
                //alert("success")
                
				$A.util.addClass(assign1, 'slds-hide');
                $A.util.removeClass(assign1, 'slds-show');
                $A.util.addClass(assign2, 'slds-hide');
                $A.util.removeClass(assign2, 'slds-show');
                $A.util.addClass(assign3, 'slds-hide');
                $A.util.removeClass(assign3, 'slds-show');
                $A.util.addClass(assign4, 'slds-hide');
                $A.util.removeClass(assign4, 'slds-show');
                
				if(buttonid!= null){
                    $A.util.removeClass(assign, 'slds-hide');
                    $A.util.addClass(assign, 'slds-show');
                    $A.util.addClass(laybutton, 'slds-hide');
                    $A.util.removeClass(laybutton, 'slds-show');
                    event.preventDefault();
                
					if (listOfVenues == undefined || listOfVenues == null) {
                        return;
                    }
                    
					listOfVenues.forEach(function(record) {
                        console.log(record);
                        if (record.Id == buttonid) {
                            record.IsCustomized = true;
                        }
                    });
                    
					component.set("v.venue", listOfVenues);
                    component.set("v.eventvenueId", eventvenue.Id);
					
					$A.util.addClass(modal, 'slds-hide');
					$A.util.removeClass(modal, 'slds-show');
                }
                
				helper.resetVenueLayoutSearch(component);
            }
        });
		
        $A.enqueueAction(action);
    },
	
   /* customize : function(component,event,helper){
        debugger;
		var selectedItem = event.currentTarget;
		var initEventVenueId = selectedItem.dataset.record;
		var recordId = component.get("v.recordId");
		var eventvenueId = component.get("v.eventvenueId");
		var evt = $A.get("e.force:navigateToComponent");

		evt.setParams({
            componentDef: "c:VenueLayoutMasterComponent",
            componentAttributes: {
                initEventVenueId: initEventVenueId,
                eventvenueId : eventvenueId
                
            }
        });

        evt.fire();  
        event.preventDefault();
    },
	
      var address = component.find("address").get("v.value");
   		var urlEvent = $A.get("e.force:navigateToURL");
    	urlEvent.setParams({
      	"url":'/c/VenueLayoutApp.app'
        "eventID": recordId,
        "eventvenueId" : eventvenueId
    	});
    	urlEvent.fire();
        //evt.fire();  
        urlEvent.preventDefault();*/
    
    
     customize : function(component,event,helper){
        debugger;
        var recordId = component.get("v.recordId");
        var eventvenueId = component.get("v.eventvenueId");
        var selectedItem = event.currentTarget;
        var initEventVenueId = eventvenueId == undefined ? selectedItem.dataset.record : eventvenueId;
        eventvenueId = eventvenueId == undefined ? selectedItem.dataset.record : eventvenueId;
        /*var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef: "c:VenueLayoutMasterComponent",
            componentAttributes: {
                eventID: recordId,
                eventvenueId : eventvenueId
                
            }
        });
        evt.fire();  
        event.preventDefault();*/
       var url  = $A.get("$Label.c.VENUELAYOUT_APP_URL");
        console.log(url);
     var urlEvent = $A.get("e.force:navigateToURL");
     urlEvent.setParams({
       "url": url+'?eventID='+recordId+'&eventvenueId='+eventvenueId
     });
     urlEvent.fire();
        //evt.fire();  
        urlEvent.preventDefault();
         event.preventDefault();
        
    },

		handleApplicationEvent:function(component,event,helper){
        
		var target = event.getParam("target");
        if(target === 'venuelayout'){
            helper.doInitHelper(component,event);
           
        }
    },
    
    modalfunctionno:function(component,event,helper){
        var modal= component.find("modal");
        $A.util.addClass(modal, 'slds-hide');
        $A.util.removeClass(modal, 'slds-show');
    }
   
})