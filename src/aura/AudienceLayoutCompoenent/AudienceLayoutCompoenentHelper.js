({
	drawAudienceVenueLayout : function(component,event,helper) {
        debugger;
        
        var globalID = component.getGlobalId();
        console.log(component.getGlobalId());
        var numberOfSeats = component.get("v.wrapperObj.NumberOfSeats");
 		
        /* Edit Layout
           Added by: Ankush
           Date    : 11/07/2017 */
        
       var obj =  component.get("v.wrapperObj");
       var stion = component.get("v.wrapperObj.sectionsType");
        console.log("obj+++++++" +obj);
       var parentdiv = document.getElementById(globalID); 
       parentdiv.addEventListener("click",$A.getCallback(function myFunction(component,event){
       var sObjectEvent = $A.get("e.c:EditLayoutEvent");
       		sObjectEvent.setParams({
            	"message" : obj,
                "sections":	stion
            });
        sObjectEvent.fire();
        }));
        
        
       /*Edit Layout End*/ 
        
        var tableName = component.get("v.wrapperObj.tableName");
        // Audience
        var totalSeats = component.get("v.wrapperObj.NumberOfSeats") 
        var capacity = Math.round(totalSeats/stion);  ;//50;
        
        var seatsPerRow = component.get("v.wrapperObj.SeatsPerRow");//10;
        var numberOfRows = Math.round(capacity/seatsPerRow);
       
        console.log("capacity" + capacity);
        console.log("seatsPerRow" + seatsPerRow);
        console.log("numberOfRows" + numberOfRows);
        console.log("remainingSeatsCount" + remainingSeatsCount);
        
        //var sections = component.get("v.section");
        var remainingSeatsCount = (totalSeats % stion);
        var columns = 1;
		var rowList = [];
         //var sequence = 1;
        //var row = 1;
        var totalSeatsOfLayout = 1;
        for(var sec=0;sec<stion;sec++){
        	
            var section = document.getElementById(globalID + '_section');
            var rowTemplate = document.getElementById(globalID + '_seat-template');
            var sectionLi = document.createElement('li');
            sectionLi.id = sec + '_sectionLi';
            sectionLi.className = 'sectionLi';
			rowTemplate.appendChild(sectionLi);            
            var sectionUl =  document.createElement('ul');
            var sectionRowTemplate = document.getElementById(sec + '_sectionLi');
            sectionUl.id = sec + '_sectionUl';
            sectionRowTemplate.appendChild(sectionUl);
            
            if(sec == stion-1){
             		capacity = capacity+ remainingSeatsCount;
             }else{
            		capacity = capacity;
        	 }
            var seatsPerRowVar = 0; 
            var seatCountPerRow = 1;
            var seatsPerSectionVar = 1;
          	for(var cap=0;cap<numberOfRows;cap++){
			
                var newSection = document.getElementById(sec + '_sectionUl');
                var rowLi = document.createElement('li');
                rowLi.id = globalID + '_rowLi';
                var rowUl = document.createElement('ul');
                rowUl.id = globalID + '_rowUl';
                rowUl.style.margin = '3px';
                newSection.appendChild(rowLi);

                for(var row=0;row<seatsPerRow;row++){
                    if(seatsPerSectionVar <= capacity && totalSeatsOfLayout <= numberOfSeats){
                     	
                        var seatLi = document.createElement('li');
                        seatLi.className = 'chair';
                        seatLi.id = tableName+'_S'+totalSeatsOfLayout;
						var label = document.createElement('label')
                        label.htmlFor = "seatLabel"+seatsPerSectionVar;
                        label.className = "seatLabel";
                        label.innerHTML = 'S'+seatsPerSectionVar;
                        seatLi.appendChild(label);
                         $(seatLi).on('dragover', function (event) {
                        console.log('ondragover0 called...');
                        event.preventDefault();
                    });
                        $(seatLi).on('drop', function (event) {
			                debugger;
			                var sObjectEvent1;
			                var obje = {};
			          		var status = $(this).attr('class');
			                if(status.indexOf("booked") <= 0){
			                    var listitem = '';
			                    if(event.originalEvent != undefined && event.originalEvent != null){
			                   	listitem = event.originalEvent.dataTransfer.getData("obj");    
			                        obje = JSON.parse(listitem);
			                    }
			                    var contactId = obje.Id;
			                    var contactName = obje.Name;
			                    component.set("v.afterRemove",contactName);
			                    var seatNo = $(this).find('label').text();
			                    var tableNo = component.get("v.wrapperObj.tableName");
			                    var seat = tableNo + '_'+ seatNo;
			                    var conSeat = contactName + '[' + seat + ']';
			                    component.set("v.afterRemove",conSeat);
			                      $(this).addClass('booked');
			                    $(this).addClass('bookedseat');
			                    $(this).attr('data','booked');
			                    $(this).attr('contact',contactId);
			                    var conList = component.get("v.contactList");
			                    for(var i = 0;i<conList.length;i++){
			                        if(conList[i].Name == contactName){
			                            conList[i].Name = contactName + '[' + seat + ']';
			                            conList[i].Title = seat;
			                        }
			                    }
			                    $(this).append('<span class="orangeBox ' + seat + '">X</span>');
			                    
			                    var spanId = document.getElementsByClassName(seat);
			                    console.log('spanId'+spanId);
			                    
			                     $(this).hover(
			                      function () {
			                        $(spanId).show();
			                      }, 
			                      function () {
			                        $(spanId).hide();
			                      }
			                    );
			                    
								$(spanId).on('click' ,function(){
			                    	//alert('Hello');
			                        debugger;
			                        $(this).parent().removeClass('booked');
			                        $(this).parent().removeClass('bookedseat');
			                        $(this).parent().removeAttr('data');
			                        $(this).parent().remove('mouseenter');
			                        $(this).parent().removeClass('orangeBox');
			                        $(spanId).remove();
			
			                        var a = $(this).parent();
			                        var contactID = a.attr("contact")
			                        var conList = component.get("v.contactList");
			                    	for(var i = 0;i<conList.length;i++){
			                        	if(conList[i].Id == contactID){
			                            	//conList[i].Name = 'WWWWWW';//Mustafa will look into it
			                            	//conList[i].Title = '';
			                        	}
			                    	}
			                        //$(this).parent().trigger("drop");
			                                                 
			                         sObjectEvent1 = component.getEvent("getAssignSeat");
			                    
			                            sObjectEvent1.setParams({
			                                "contactId":contactId,
			                                "seat" :seat,
			                                "tableNo":tableNo,
			                                "status":"Cancelled"
			                               })
			                           sObjectEvent1.fire();
			                      });
			                     sObjectEvent1 = component.getEvent("getAssignSeat");
			                   var status = "Registered";
			                    sObjectEvent1.setParams({
			                        "contactId":contactId,
			                        "seat" :seat,
			                        "tableNo":tableNo,
			                        "status":status
			                       })
			                   sObjectEvent1.fire();
			                }
			                else{
			                    alert("Sorry ! already booked...");
			                }
			             
			            });
                       
                        rowUl.appendChild(seatLi);
                    	seatsPerRowVar++;
                    	seatsPerSectionVar++;
                        totalSeatsOfLayout++;
                    }
                }
                rowLi.appendChild(rowUl);
                var seatTemplate = $("ul[id='"+sec+"_sectionUl"+"']");
                seatTemplate.append(rowLi);
           		seatCountPerRow++;
           		this.eventRegistrationContactRound(component, event,helper);
            }
        }
    },
    
    eventRegistrationContactRound : function(component,event,helper){
        debugger;
         var tableName = component.get("v.wrapperObj.tableName");
         var globalID = component.getGlobalId();
      
        var eventRegistrationValues = component.get("v.eventRegistrationValues");

        if(eventRegistrationValues!=null){
          for(var i=0; i< eventRegistrationValues.length;i++){
        
	        var childId =   document.getElementById(eventRegistrationValues[i].Seat__c);
	        if(childId != null){
	            
	            childId.style.backgroundColor = "#D5A69B";
	        }else{
	            $(this).css('background-color','#D5A69B');
	        }
          }
        }
     }
  
})