({
	 doInit: function(component, event, helper){
        var action = component.get("c.getLayoutTypes");
        var inputsel = component.find("InputSelect");
      var opts=[];
    action.setCallback(this, function(a) {
        opts.push({"class": "optionClass", label:'--Select--', value: '--Select--'});
        for(var i=0;i< a.getReturnValue().length;i++){
            opts.push({"class": "optionClass", label: a.getReturnValue()[i].DeveloperName, value: a.getReturnValue()[i].DeveloperName});
        }
        inputsel.set("v.options", opts);

    });
    $A.enqueueAction(action); 

		
	},
    
    
    
    onChangeLayoutFunction : function(component, event, helper){
        debugger
		var inputsel = component.find("InputSelect").get("v.value");
		var assign= component.find("form1");
       var text= component.find("text");
       $A.util.removeClass(assign, 'slds-hide');
		$A.util.addClass(assign, 'slds-show');
		$A.util.addClass(text, 'slds-hide');
		$A.util.removeClass(text, 'slds-show');  
		if(inputsel == "Custom"){
			var action = component.get("c.getCustomLayoutTypes");
		}else if(inputsel == "Plain"){
			var action = component.get("c.getPlainLayoutTypes");
		}else{
			return null;
		}
		
		var opts=[];
		action.setCallback(this, function(a) {
			opts.push({"class": "optionClass", label:'--Select--', value: '--Select--'});
			for(var i=0;i< a.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: a.getReturnValue()[i].Label, value: a.getReturnValue()[i].DeveloperName});
			}
			component.find("type").set("v.options", opts);

		});
		$A.enqueueAction(action); 
    },
    onChangeType : function(component, event, helper){
        debugger;
         
        var inputsel = component.find("type").get("v.value");
        var text= component.find("text");
        $A.util.removeClass(text, 'slds-hide');
        $A.util.addClass(text, 'slds-show');
		component.set("v.inputsel", inputsel);
      		if(inputsel == 'ConferenceHall'){
            helper.calconference(component, event, helper);
          
        }
		
        
    },
    
    
    
     CreateVenueLayout : function(component,event,helper){
         debugger;
        
        var appEvent = component.getEvent("appEvent");


         var flag=component.get("v.validateflag");
         var layout = component.get("v.venuelayout");
          var eventvenueId = component.get("v.eventvenueId");
        var action = component.get("c.saveVenueLayout");
          helper.validatefeilds(component, event,helper);
        var flag=component.get("v.validateflag");
           if(flag == "false"){
         action.setParams({ "eventvenueId" : eventvenueId, "layout" : layout});
        
        action.setCallback(this, function(response) {
            debugger;
            var state = response.getState();
            var results = response.getReturnValue();
            if (state === "SUCCESS") {
               

              
                
               appEvent.setParams({
            "wrapper" : results  ,
                   "reRenderFlag" : true  
            });
        appEvent.fire();
                helper.resetdata(component, event,helper);
               
               
            }
            
            
            
        });
        $A.enqueueAction(action);
                 
           }
         
       
    }
})