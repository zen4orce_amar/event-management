({
   doInitHelper: function(component, event, helper){
    
     var action = component.get("c.getVenueLayoutType");
        var venueLayoutId = component.get("v.eventvenueId");
        var reRenderFlag = component.get("v.reRenderFlag");
         var wrapperRender = component.get("v.wrapperRender");
        console.log("eventID" + component.get("v.eventID"));
        this.eventRegistrationContact(component, event,helper);
        action.setParams({ "venueLayoutId": venueLayoutId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'ERROR') {
                console.log(a.getError());
            } else if (state === 'SUCCESS') {
                debugger;
                if(reRenderFlag == "false"){
                component.set("v.wrapperList", response.getReturnValue());
                console.log(component.get("v.wrapperList"));
                }else{
                  component.set("v.wrapperList", wrapperRender);  
                    
                }
                var eventRegistrationList = component.get("v.eventRegistrationList");
                var tableList = response.getReturnValue().tables;
                if(tableList.length != 0){
                     conferenceType = response.getReturnValue().tables[0].defaultConferenceType != null ? response.getReturnValue().tables[0].defaultConferenceType : ''; 
                }else{
                    conferenceType = '';
                }
                console.log("tableList" + tableList);
                var rowList = [];
                var columnList = [];
                var columns = response.getReturnValue().SeatsPerRow;
                var rows = response.getReturnValue().numberOfTables / columns;
                //if(rows < rows+.5 ){
                //    rows++;
                //}
                var defaultlayoutType = response.getReturnValue().defaultlayoutType;
                var totalSeats = response.getReturnValue().totalSeats;
                var SeatsPerRow = response.getReturnValue().SeatsPerRow;
                var Sections = response.getReturnValue().Sections;
                for (var i = 0; i < rows; i++) {
                    rowList.push(i);
                }
                for (var j = 0; j < columns; j++) {
                    columnList.push(j);
                }
                
                component.set("v.numberOfRows", rowList);
                component.set("v.numberOfColumns", columnList);
                var rowListData = [];
                var counter = 1;
                var registeredSeatIndex = 0;
                for (var row = 0; row < rows; row++) {
                    for (var column = 0; column < columns; column++) {
                          if (counter <= tableList.length) {
                            if(eventRegistrationList[registeredSeatIndex] !== undefined && eventRegistrationList[registeredSeatIndex] !== null) {
                                var isTableRegistered = false;
                                for (var index = 0; index < eventRegistrationList.length; index++) {
                                    if(tableList[counter-1] != undefined && tableList[counter-1].tableName == eventRegistrationList[index].key) {
                                        rowListData.push({ "rowNo": row, "colNo": column, "table": tableList[counter-1], "defaultlayoutType": defaultlayoutType, "totalSeats": totalSeats, "SeatsPerRow": SeatsPerRow, "Sections": Sections ,"registeredContacts": eventRegistrationList[index].value});
                                        isTableRegistered = true;
                                        //counter++;
                                        //registeredSeatIndex++;
                                    } /*else {
                                       rowListData.push({ "rowNo": row, "colNo": column, "table": tableList[counter-1], "defaultlayoutType": defaultlayoutType, "totalSeats": totalSeats, "SeatsPerRow": SeatsPerRow, "Sections": Sections ,"registeredContacts": ''}); 
                                       
                                    }*/
                                }
                                if (isTableRegistered == false) {
                                    rowListData.push({ "rowNo": row, "colNo": column, "table": tableList[counter-1], "defaultlayoutType": defaultlayoutType, "totalSeats": totalSeats, "SeatsPerRow": SeatsPerRow, "Sections": Sections ,"registeredContacts": ''}); 
                                }
                                counter++;
                            } else {
                                   rowListData.push({ "rowNo": row, "colNo": column, "table": tableList[counter-1], "defaultlayoutType": defaultlayoutType, "totalSeats": totalSeats, "SeatsPerRow": SeatsPerRow, "Sections": Sections ,"registeredContacts": ''}); 
                                   counter++;
                            }
                        }
                    }
                }
                console.log(rowListData);
                component.set("v.tempList", rowListData);
               
            }
			
			var body = document.getElementById('body');
			var outer = document.getElementById('outer');
			var zoomIn = document.getElementById('zoomIn');
			var zoomOut = document.getElementById('zoomOut');
			var reset = document.getElementById('reset');
			var currentZoom = 1.0;
			
			$(zoomIn).click(function ZoomIn (event) {
			
				$(outer).animate({ 'zoom': currentZoom += .1 }, 'slow');
			});

			$(zoomOut).click(function  ZoomOut (event) {
			
				 $(outer).animate({ 'zoom':  currentZoom -= .1 }, 'slow');
			});
			$(reset).click(function () {
				currentZoom = 1.0
				$(outer).animate({ 'zoom': 1 }, 'slow');
			 });
			
        });
		
		
        $A.enqueueAction(action);
   },
   
   showModal : function(component) {
        
        document.getElementById("modalId").style.display = "block";
     
    },
    
    
    eventRegistrationContact : function(component,event,helper){
          var eventvenueId = component.get("v.eventvenueId");
        console.log('Init venueLayoutController');
        //component.set("v.flag",true);
     
        var action = component.get("c.getEventRegistrationContact");
        action.setParams({ "eventvenueId" : eventvenueId});
        
        action.setCallback(this, function(response) {
            debugger;
            var state = response.getState();
            var results = response.getReturnValue();
            if (state === "SUCCESS") {
             
                 console.log('results'+results);
                
                 var custs = [];
                 for ( key in results ) {
                    custs.push({value:results[key], key:key});
                }
                component.set("v.eventRegistrationList", custs);
               
            }
            
            
            
        });
        $A.enqueueAction(action);
      
    }
     
    

})