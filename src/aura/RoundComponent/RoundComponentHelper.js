({
    drawDefaultVenueLayout : function(component,event,helper) {
        debugger;
        var $j= jQuery.noConflict();
        var globalID = component.getGlobalId();
        console.log(component.getGlobalId());
        var numberOfSeats = component.get("v.wrapperObj.NumberOfSeats");
        var tableName = component.get("v.wrapperObj.tableName");
        var tableId = component.get("v.tableId");
        var parentdiv = document.createElement('div');
        var obj = component.get("v.wrapperObj");
        parentdiv.setAttribute("style","position: relative; width: 120px; height: 120px; background-color: rgb(205, 133, 63);border-radius: 50% ");
        parentdiv.id = tableName+component.getGlobalId();
        parentdiv.className = 'testClass';
        parentdiv.addEventListener("click",$A.getCallback(function myFunction(component,event){
            var sObjectEvent = $A.get("e.c:EditLayoutEvent");
            sObjectEvent.setParams({
                "message": obj
            })
            sObjectEvent.fire();
        }));
        $("div[id='"+globalID+"']").append(parentdiv);
        var height = document.getElementById(tableName+globalID).offsetHeight;
        var width = document.getElementById(tableName+globalID).offsetWidth;
        var div = 360 / numberOfSeats;
        var radius = 90;
        if(numberOfSeats <= "10"){
            radius = 90;
        }else if(numberOfSeats <= "20"){
            radius = 135;
            parentdiv.setAttribute("style","position: relative; width: 180px; height: 180px; background-color: rgb(205, 133, 63);border-radius: 50% ");
        }else if(numberOfSeats <= "30"){
            parentdiv.setAttribute("style","position: relative; width: 270px; height: 270px; background-color: rgb(205, 133, 63);border-radius: 50% ");
            // document.getElementById(globalID).setAttribute("style","width:270px;height:270px");
            radius = 180; 
        }else if(numberOfSeats <= "40"){
            
            parentdiv.setAttribute("style","position: relative; width: 360px; height: 360px; background-color: rgb(205, 133, 63);border-radius: 50% ");
            // document.getElementById(globalID).setAttribute("style","width:270px;height:270px");
            radius = 180; 
        }
        var offsetToParentCenter = parseInt(parentdiv.offsetWidth / 2);  //assumes parent is square
        var offsetToChildCenter = 20;
        var totalOffset = offsetToParentCenter - offsetToChildCenter;
        var obje ;
        for (var j = 1; j <= numberOfSeats; ++j){
            var childdiv = document.createElement('div');
            childdiv.className = 'div2';
            childdiv.setAttribute("style","position: absolute;width: 40px;height: 40px;background-color: #FFFFFF;border-radius: 100px;border: 1px dashed #060606;");
           // childdiv.id = numberOfSeats;
            childdiv.id = tableName+'_S'+j;
            // childdiv.style.position = 'absolute';
            var y = Math.sin((div * j) * (Math.PI / 180)) * radius;
            var x = Math.cos((div * j) * (Math.PI / 180)) * radius;
            childdiv.style.top = (y + totalOffset).toString() + "px";
            childdiv.style.left = (x + totalOffset).toString() + "px";
            parentdiv.append(childdiv);
            var seatNumber = document.createElement('p');
            seatNumber.className = 'p'
            seatNumber.innerHTML = 'S'+j;
            childdiv.append(seatNumber);
            
            $(childdiv).on('dragover', function (event) {
                console.log('ondragover0 called...');
                event.preventDefault();
            });
            var childDivValue = document.getElementById(childdiv.id ).value 
            $(childdiv).on('drop', function (event) {
                 debugger;
                var sObjectEvent1;
                var obje = {};
         		var status = $(this).attr('data');
                if(status != 'booked'){
                    var listitem = '';
                   if(event.originalEvent != undefined && event.originalEvent != null){
                    	listitem = event.originalEvent.dataTransfer.getData("obj");    
                        obje = JSON.parse(listitem);
                     }
                    var contactId = obje.Id;
                    var contactName = obje.Name;
                    component.set("v.afterRemove",contactName);
                    var seatNo = $(this).find('p').text();
                    var tableNo = component.get("v.wrapperObj.tableName");
                    var seat = tableNo + '_'+ seatNo;
                    var conSeat = contactName + '[' + seat + ']';
                    component.set("v.afterRemove",conSeat);
                    $(this).addClass('bookedseat');
                    $(this).attr('data','booked');
                    $(this).attr('contact',contactId);
                    var conList = component.get("v.contactList");
                    for(var i = 0;i<conList.length;i++){
                        if(conList[i].Name == contactName){
                            conList[i].Name = contactName + '[' + seat + ']';
                            conList[i].Title = seat;
                        }
                    }
                    $(this).append('<span Name="' + seat + '" class="orangeBox">X</span>');
                    
                    var spanId = document.getElementsByName(seat);
                   console.log('spanId'+spanId);
                    
                     $(this).hover(
                      function () {
                        $(spanId).show();
                      }, 
                      function () {
                        $(spanId).hide();
                      }
                    );
                    
					$(spanId).on('click' ,function(){
                    	//alert('Hello');
                        debugger;
                       $(this).parent().removeClass('bookedseat');
                        $(this).parent().removeAttr('data');
                        $(this).parent().unbind('mouseenter');
                        $(this).parent().removeClass('orangeBox');
                       $(spanId).remove();

                       var a = $(this).parent();
                        var contactID = a.attr("contact")
                       var conList = component.get("v.contactList");
                    	for(var i = 0;i<conList.length;i++){
                        	if(conList[i].Id == contactID){
                            	//conList[i].Name = 'WWWWWW';//Mustafa will look into it
                            	//conList[i].Title = '';
                        	}
                    	}
                        //$(this).parent().trigger("drop");
                                                
                         sObjectEvent1 = component.getEvent("getAssignSeat");
                   
                            sObjectEvent1.setParams({
                                "contactId":contactId,
                                "seat" :seat,
                               "tableNo":tableNo,
                                "status":"Cancelled"
                               })
                          sObjectEvent1.fire();
                      });
                     sObjectEvent1 = component.getEvent("getAssignSeat");
                    var status = "Registered";
                    sObjectEvent1.setParams({
                        "contactId":contactId,
                        "seat" :seat,
                        "tableNo":tableNo,
                       "status":status
                       })
                   sObjectEvent1.fire();
                }
               else{
                    alert("Sorry ! already booked...");
                 }
                
            });
            
        }
        var tableNumber = document.createElement('p1');
        tableNumber.innerHTML =tableName;
        tableNumber.className = 'p1';
        parentdiv.append(tableNumber); 
        this.eventRegistrationContactRound(component, event,helper);
    },
    
    eventRegistrationContactRound : function(component,event,helper){
        debugger;
         var tableName = component.get("v.wrapperObj.tableName");
         var globalID = component.getGlobalId();
      
        var eventRegistrationValues = component.get("v.eventRegistrationValues");

        if(eventRegistrationValues!=null){
          for(var i=0; i< eventRegistrationValues.length;i++){
        
	        var childId =   document.getElementById(eventRegistrationValues[i].Seat__c);
	        if(childId != null){
	            
	            childId.style.backgroundColor = "#D5A69B";
	        }else{
	            $(this).css('background-color','#D5A69B');
	        }
          }
        }
     }
})